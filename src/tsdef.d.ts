type LineConfig = {
    name: string,
    password: string,
    sipProxyName: string,
    webrtc: boolean
}

type SoftPhoneConfig = {
    clientName: string,
    websocketPort: number,
    token: string,
    audioTagID: string,
    sslEnabled: boolean,
    websocketDomain?: string
}

type PhoneActionResult = {
    success: boolean,
    error?: string
}

interface Phone {
    dial(destination: string, sessionStateCallback: (newState: any) => any): Promise<PhoneActionResult>
    answer(callid: number): Promise<PhoneActionResult>
    hangup(callid: number): Promise<PhoneActionResult>
    transfert(callid: number, destination: string): Promise<PhoneActionResult>
    threePartyConference(ongoingCallid: number, holdedCallid: number): Promise<PhoneActionResult>
    hold(callid: number): Promise<PhoneActionResult>
    dtmf(key: string): Promise<PhoneActionResult>
}

interface Window {
    Cti: any
    PhoneController: any
}