/// <reference path="../tsdef.d.ts" />
import { Invitation, Inviter, SessionState, URI, UserAgent } from "sip.js";

export class Softphone implements Phone {

  private websocketProtocol: string
  private websocketPath: string
  private audioTagID: string
  private clientName: string
  private websocketDomain: string | undefined
  private websocketPort: number
  private token: string
  private userAgent: UserAgent
  private phoneController: any

  constructor(softPhoneConfig: SoftPhoneConfig, lineConfig: LineConfig) {
      this.websocketProtocol = softPhoneConfig.sslEnabled ? 'wss://' : 'ws://'
      this.websocketPath = softPhoneConfig.sslEnabled ? 'wssip' : 'ws'
      this.websocketDomain = softPhoneConfig.websocketDomain
      this.websocketPort = softPhoneConfig.websocketPort
      this.audioTagID = softPhoneConfig.audioTagID ? softPhoneConfig.audioTagID : "audio_remote"
      this.clientName = softPhoneConfig.clientName
      this.token = softPhoneConfig.token

      this.phoneController = window.Cti
      console.log(`Softphone created with following conf : ${JSON.stringify(softPhoneConfig)}}`)

      this.userAgent = this.createUserAgent(lineConfig)
      this.userAgent.start()
      console.log(`UserAgent started with following conf : ${JSON.stringify(lineConfig)}`)

      this.phoneController.setHandler(this.phoneController.MessageType.WEBRTCCMD, this.processPhoneCtrlCommand);
  }

  private processPhoneCtrlCommand(msg: {command: string, sipCallId: number, key: string}): void {
    switch(msg.command) {
      case "Answer":
        this.answer(msg.sipCallId);
        break;
      case  "Hold":
        this.hold(msg.sipCallId);
        break;
      case "SendDtmf":
        this.dtmf(msg.key);
        break;
      case "ToggleMicrophone":
        this.toggleMicrophone(msg.sipCallId);
        break;
      default:
        console.log("Missing softphone method for following command : ", msg.command);
      }
  }

  private createUserAgent(lineConfig: LineConfig): UserAgent {
    return new UserAgent({
      uri: UserAgent.makeURI(`sip:${lineConfig.name}@${lineConfig.sipProxyName}`),
      authorizationPassword: 'todo?',
      authorizationUsername: 'todo?',
      delegate: {
        onInvite: this.onInvite
      },
      transportOptions: {
        server: this.getWebsocketServer()
      }
    })
  }

  private createTarget(destination: string): URI {
    let targetUri = UserAgent.makeURI(`${destination}@example.com`)
    if (!targetUri) throw new Error(`Could not generate URI`)
    return targetUri
  }

  private createInviter(userAgent: UserAgent, target: URI): Inviter {
    return new Inviter(userAgent, target, {
      sessionDescriptionHandlerOptions: {
        constraints: {audio: true, video: false}
      }
    })
  }

  private getWebsocketServer(): string {
    return `${this.websocketProtocol}${this.websocketDomain}`
  }

  // @ts-ignore: To be implemented
  private onInvite(invite: Invitation) {
  }

  public async dial(destination: string, sessionStateCallback: (newState: SessionState) => any): Promise<PhoneActionResult> {
    let target = this.createTarget(destination)
    let inviter = this.createInviter(this.userAgent, target)

    inviter.stateChange.addListener(sessionStateCallback);

    return inviter.invite()
    .then(() => {
      return {success: true}
    })
    .catch((error: Error) => {
      return {success: false, error: error.message}
    });
  }

  // @ts-ignore: To be implemented
  public async answer(callid: number): Promise<PhoneActionResult> {
  }

  // @ts-ignore: To be implemented
  public async hangup(callid: number): Promise<PhoneActionResult> {

  }

  // @ts-ignore: To be implemented
  public async transfert(callid: number, destination: string): Promise<PhoneActionResult> {

  }

  // @ts-ignore: To be implemented
  public async threePartyConference(ongoingCallid: number, holdedCallid: number): Promise<PhoneActionResult> {

  }

  // @ts-ignore: To be implemented
  public async hold(callid: number): Promise<PhoneActionResult> {

  }

  // @ts-ignore: To be implemented
  public async toggleMicrophone(callId: number): Promise<PhoneActionResult> {

  }
  
  // @ts-ignore: To be implemented
  public async dtmf(key: string): Promise<PhoneActionResult> {

  }

}