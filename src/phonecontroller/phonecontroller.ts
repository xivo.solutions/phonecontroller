/// <reference path="../tsdef.d.ts" />
import { Deskphone } from '../deskphone/deskphone'
import { Softphone } from '../softphone/softphone'

export class PhoneController {

    public phone: Phone

    constructor(lineConfig: LineConfig, softphoneConfig?: SoftPhoneConfig){
        console.log(lineConfig.webrtc, softphoneConfig != undefined)
        switch (lineConfig.webrtc) {
            case true:
                if (softphoneConfig) this.phone = new Softphone(softphoneConfig, lineConfig)
                else throw new Error(`Initializing a webrtc line requires a softphone configuration`)
                break
            case false:
                // @ts-ignore: Deskphone to be implemented
                this.phone = new Deskphone()
                break
        }
    }
}

window.PhoneController = PhoneController