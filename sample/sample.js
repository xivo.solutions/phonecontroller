var phoneController = new PhoneController(
    {
        hasDevice: false,
        id: "499",
        isUa: false,
        name: "somename",
        password: "somepassword",
        sipProxyName: "default",
        vendor: null,
        webrtc: true,
        xivoIp: "someip"
    },
    {
        clientName: "XiVO Assistant",
        websocketPort: "443",
        token: "sometoken",
        audioTagID: "audio_remote",
        sslEnabled: true,
        websocketDomain: "somedomain"
    }
)

var onState = (state) => {
    console.log(state)
}

phoneController.phone.dial('*55', onState)