import { Softphone } from '../src/softphone/softphone'
let softphone = undefined

test('It should instanciate a softphone', () => {
  softphone = new Softphone({
    clientName: "Application 1",
    websocketPort: 443,
    token: "token-test-abcd",
    audioTagID: "audio-dom-element",
    sslEnabled: true,
    websocketDomain: "website.com/wsdomain"
  },{
    name: "1010",
    password: "asdvgbnjklmputr",
    sipProxyName: "default",
    webrtc: true
  })
  expect(softphone).toBeDefined()
});
